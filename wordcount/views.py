from django.http import HttpResponse
from django.shortcuts import render
import operator

def homepage(request):    
    return render (request, 'home.html')

def countpage(request):

    full_text = request.GET['text-input']
    word_list = full_text.split()
    total_count = len(word_list)
    word_list_count = {}

    for word in word_list:
        if word in word_list_count:
            word_list_count[word] += 1
        else:
            word_list_count[word] = 1

    sorted(word_list_count.items(), reverse = True)  

    return render (request, 'count.html', { 'full_text' : full_text, 'total_count' : total_count, 'word_list_count' : word_list_count.items()})



